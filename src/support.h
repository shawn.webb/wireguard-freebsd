/* SPDX-License-Identifier: ISC
 *
 * Copyright (C) 2021 Jason A. Donenfeld <Jason@zx2c4.com>. All Rights Reserved.
 * Copyright (C) 2021 Matt Dunwoodie <ncon@noconroy.net>
 */

#ifndef _WG_SUPPORT
#define _WG_SUPPORT

#include <sys/types.h>
#include <sys/limits.h>
#include <sys/endian.h>
#include <sys/socket.h>
#include <sys/libkern.h>
#include <sys/malloc.h>
#include <sys/proc.h>
#include <sys/lock.h>
#include <sys/smp.h>
#include <sys/gtaskqueue.h>
#include <vm/uma.h>

/* TODO the following is openbsd compat defines to allow us to copy the wg_*
 * files from openbsd (almost) verbatim. this will greatly increase maintenance
 * across the platforms. it should be moved to it's own file. the only thing
 * we're missing from this is struct pool (freebsd: uma_zone_t), which isn't a
 * show stopper, but is something worth considering in the future.
 *  - md */

#define rw_assert_wrlock(x) rw_assert(x, RA_WLOCKED)
#define rw_enter_write rw_wlock
#define rw_exit_write rw_wunlock
#define rw_enter_read rw_rlock
#define rw_exit_read rw_runlock
#define rw_exit rw_unlock

#define RW_DOWNGRADE 1
#define rw_enter(x, y) do {		\
	CTASSERT(y == RW_DOWNGRADE);	\
	rw_downgrade(x);		\
} while (0)

MALLOC_DECLARE(M_WG);

#include <crypto/siphash/siphash.h>
typedef struct {
	uint64_t	k0;
	uint64_t	k1;
} SIPHASH_KEY;

static inline uint64_t
siphash24(const SIPHASH_KEY *key, const void *src, size_t len)
{
	SIPHASH_CTX ctx;

	return (SipHashX(&ctx, 2, 4, (const uint8_t *)key, src, len));
}

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#endif

#ifndef PRIV_NET_WG
#define PRIV_NET_WG PRIV_NET_HWIOCTL
#endif

#ifndef IFT_WIREGUARD
#define IFT_WIREGUARD IFT_PPP
#endif

int
sogetsockaddr(struct socket *so, struct sockaddr **nam);

#if __FreeBSD_version < 1300000
#define taskqgroup_attach(a, b, c, d, e, f) taskqgroup_attach(a, b, c, 0, f)
#define taskqgroup_attach_cpu(a, b, c, d, e, f, g) taskqgroup_attach_cpu(a, b, c, d, 0, g)
#undef NET_EPOCH_ENTER
#define NET_EPOCH_ENTER(et) NET_EPOCH_ENTER_ET(et)
#undef NET_EPOCH_EXIT
#define NET_EPOCH_EXIT(et) NET_EPOCH_EXIT_ET(et)
#define NET_EPOCH_CALL(f, c) epoch_call(net_epoch_preempt, (c), (f))
#define NET_EPOCH_ASSERT() MPASS(in_epoch(net_epoch_preempt))
#undef atomic_load_ptr
#define atomic_load_ptr(p) (*(volatile __typeof(*p) *)(p))

struct taskqgroup_cpu {
	LIST_HEAD(, grouptask)	tgc_tasks;
	struct gtaskqueue	*tgc_taskq;
	int	tgc_cnt;
	int	tgc_cpu;
};

struct taskqgroup {
	struct taskqgroup_cpu tqg_queue[MAXCPU];
	struct mtx	tqg_lock;
	const char *	tqg_name;
	int		tqg_adjusting;
	int		tqg_stride;
	int		tqg_cnt;
};


static inline void taskqgroup_drain_all(struct taskqgroup *tqg)
{
	struct gtaskqueue *q;

	for (int i = 0; i < mp_ncpus; i++) {
		q = tqg->tqg_queue[i].tgc_taskq;
		if (q == NULL)
			continue;
		gtaskqueue_drain_all(q);
	}
}
#endif

#endif
